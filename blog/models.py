from django.db import models

# Create your models here.

class Sitio(models.Model):
    identificador= models.IntegerField()
    nombre= models.CharField(max_length=100)
    descripcion= models.TextField()
    latitud= models.DecimalField(max_digits=10, decimal_places=7)
    longitud= models.DecimalField(max_digits=10, decimal_places=7)
    url= models.CharField(max_length=100)